const Telegraf = require('telegraf');
const session = require('telegraf/session');
const fs = require('fs');
const path = require('path');
const config = require('./config/config.json');
const bot = new Telegraf(config.token);
const dbDir = 'database';
const dbFile = 'db.json';
const db = JSON.parse(fs.readFileSync(path.join(__dirname, dbDir, dbFile)));

bot.use(session());

const messages = {
    startingVote: `Вжууух!\nНачинается магия... 💃`,
    error: `Упс, что-то случилось...\nПопробуй ещё раз! 🤷‍♀️`,
    errorRetry: `Упс, что-то случилось...\nЗапускаю ещё раз! 🤷‍♀️`,
    successRun: `Детектор принцессок успешно запущен! 💁‍♀️`,
    errorRunETA: `Можно запускать детектор принцессок только раз в сутки.\nПодожди ещё (ч): <strong>%s</strong>! 🙅‍♀️`,
    successJoin: `Теперь у тебя есть шанс стать принцесской, %s! 🙋‍♀️`,
    alreadyJoin: `Ты уже участвуешь, %s! 🙅‍♀️`,
    successBack: `Хорошо, что ты вернулась, %s! 🙋‍♀️`,
    successLeave: `Теперь ты не будешь принцесской, %s! 🙍‍♀️`,
    alreadyLeave: `Ты перестала участвовать раньше, %s! 🙅‍♀️`,
    successReset: `Принцесски были удалены! 💇‍♀️`,
    playersWithScoresNotFound: `Детектор принцессок не обнаружил участниц со счётом! 🤷‍♀️️`,
    playersNotFound: `В этом чате ещё никто не захотел быть принцесской! 🤷‍♀️`,
    accessDenied: `Прости, дорогуша, но тебе сюда нельзя, %s! 🙅‍♀️`,
    hasNotData: `У детектора принцессок нет никакой информации об этом чате! 🤷‍♀️`,
    greetings: `Ваше Высочество, %s! 🙋‍♀️`,
    greetingsError: `Пожалуйста добавьте бота в чат и наберите команду:\n/psinit!`,
    congrats: `Принцесска дня:\n\n👸 <strong>%name</strong>\n\nВаше Высочество, %nick!\nВы прекрасны сегодня, как никогда!`,
    top: `ТОП-10 принцессок этого чата 👸`,
    players: `Список участвующих 👸`
};

const updateDB = () => {
    return new Promise((resolve, reject) => {
        fs.writeFile(
            path.join(__dirname, dbDir, dbFile),
            JSON.stringify(db),
            error => {
                error
                    ? reject(error)
                    : resolve('DB has been updated successfully');
            }
        );
    });
};

const returnError = error => {
    return new Promise(resolve => {
        resolve(() => {
            console.log('New error:');
            console.log('Date: ', new Date());
            console.log('Error code: ', error.code);
            console.log('Error: ', error.description);
        });
    });
};

const returnUserName = (from, value = 'nick', result = '') => {
    const userName = from.username ? from.username : '';
    const firstName = from.first_name ? from.first_name : '';
    const lastName = from.last_name ? ` ${from.last_name}` : '';

    if (value === 'name') {
        result = firstName || lastName ? firstName + lastName : `@${userName}`;
    } else {
        result = userName ? `@${userName}` : firstName + lastName;
    }

    return result;
};

const getChat = (ctx, type = 'manual') => {
    return new Promise(resolve => {
        const currentChatId = ctx.update.message.chat.id;

        ctx.getChatMember(ctx.update.message.from.id)
            .then(member => {
                if (member.user.is_bot) {
                    ctx.reply(messages.accessDenied).catch(error =>
                        returnError(error)
                    );
                } else {
                    if (db.length) {
                        const chatIndex = db.findIndex(
                            ({ id }) => id === currentChatId
                        );
                        const chat = db[chatIndex];

                        if (chat) {
                            resolve({
                                chat: chat,
                                member: member
                            });
                        } else {
                            if (type === 'manual') {
                                ctx.reply(messages.hasNotData).catch(error =>
                                    returnError(error)
                                );
                            }
                        }
                    } else {
                        if (type === 'manual') {
                            ctx.reply(messages.hasNotData).catch(error =>
                                returnError(error)
                            );
                        }
                    }
                }
            })
            .catch(error => returnError(error));
    });
};

const getPlayer = (ctx, chat, member) => {
    return new Promise(resolve => {
        if (chat.players) {
            resolve(
                chat.players[
                    chat.players.findIndex(({ id }) => id === member.user.id)
                ]
            );
        } else {
            ctx.reply(messages.hasNotData).catch(error => returnError(error));
        }
    });
};

const hasPlayers = (ctx, chat, type) => {
    if (chat.players) {
        return true;
    } else {
        if (type === 'manual') {
            return ctx
                .reply(messages.hasNotData)
                .catch(error => returnError(error));
        } else {
            return false;
        }
    }
};

const isAdmin = member =>
    member.status === 'creator' || member.status === 'administrator';

const join = (ctx, chat, member) => {
    chat.players.push({
        id: member.user.id,
        name: returnUserName(member.user, 'name'),
        status: true,
        score: 0
    });

    updateDB().then(() => {
        ctx.reply(
            messages.successJoin.replace('%s', returnUserName(member.user))
        ).catch(error => returnError(error));
    });
};

const returnRandomInt = (min, max) =>
    Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min) + 1)) +
    Math.ceil(min);

const addActivePlayers = (ctx, players, list = 'top') => {
    return new Promise(resolve => {
        const activePlayers = [];

        const map = players.map(async player => {
            try {
                const member = await ctx.getChatMember(player.id);

                if (
                    member.user.id === player.id &&
                    member.status !== 'left' &&
                    member.status !== 'kicked'
                ) {
                    const push = player => {
                        player.name = returnUserName(member.user, 'name');
                        activePlayers.push(player);
                    };

                    if (list === 'top') {
                        if (player.status && player.score) push(player);
                    } else {
                        if (player.status) push(player);
                    }
                } else {
                    player.status = false;

                    await updateDB();
                }
            } catch (error) {
                await returnError(error);
            }
        });

        Promise.all(map).then(() => resolve(activePlayers));
    });
};

const printPlayers = (ctx, chat, list = 'top') => {
    if (chat.players.length) {
        addActivePlayers(ctx, chat.players, list).then(activePlayers => {
            if (activePlayers.length) {
                if (list === 'top') {
                    sort(activePlayers).then(sorted => {
                        const sortedPromise = sorted.map(
                            async (player, index) => {
                                if (index === 0) {
                                    return `${index + 1}. <strong>${
                                        player.name
                                    }</strong>: ${player.score} 👸`;
                                } else {
                                    return `${index + 1}. ${player.name}: ${
                                        player.score
                                    }`;
                                }
                            }
                        );

                        Promise.all(sortedPromise).then(async res => {
                            await ctx
                                .replyWithHTML(
                                    `<strong>${messages.top} (${sorted.length}):</strong>\n\n` +
                                        res.join('\n')
                                )
                                .catch(error => returnError(error));
                        });
                    });
                } else if (list === 'all') {
                    ctx.replyWithHTML(
                        `<strong>${messages.players} (${activePlayers.length}):</strong>\n\n` +
                            activePlayers
                                .map((player, index) => {
                                    return `${index + 1}. ${player.name}: ${
                                        player.score
                                    }`;
                                })
                                .join('\n')
                    ).catch(error => returnError(error));
                }
            } else {
                ctx.reply(messages.playersWithScoresNotFound).catch(error =>
                    returnError(error)
                );
            }
        });
    } else {
        ctx.reply(messages.playersNotFound).catch(error => returnError(error));
    }
};

const dailyVote = (ctx, chat, type) => {
    return new Promise(resolve => {
        if (chat.players.length) {
            let winner = '';

            addActivePlayers(ctx, chat.players, 'all')
                .then(activePlayers => {
                    if (activePlayers.length) {
                        winner =
                            activePlayers[
                                returnRandomInt(0, activePlayers.length)
                            ];
                    } else {
                        if (type === 'manual') {
                            chat.updated = 0;
                            updateDB().then(() =>
                                ctx
                                    .reply(messages.playersNotFound)
                                    .catch(error => returnError(error))
                            );
                        }
                    }
                })
                .then(() => {
                    if (winner) {
                        ctx.getChatMember(winner.id)
                            .then(member => {
                                ctx.reply(messages.startingVote)
                                    .then(() => {
                                        const winnerIndex = chat.players.findIndex(
                                            ({ id }) => id === member.user.id
                                        );

                                        winner = chat.players[winnerIndex];
                                        winner.name = returnUserName(
                                            member.user,
                                            'name'
                                        );
                                        winner.score++;

                                        updateDB().then(() => {
                                            ctx.replyWithHTML(
                                                messages.congrats
                                                    .replace(
                                                        '%name',
                                                        returnUserName(
                                                            member.user,
                                                            'name'
                                                        )
                                                    )
                                                    .replace(
                                                        '%nick',
                                                        returnUserName(
                                                            member.user
                                                        )
                                                    )
                                            )
                                                .then(() =>
                                                    printPlayers(ctx, chat)
                                                )
                                                .then(() =>
                                                    resolve(
                                                        'Daily vote has been successfully finished!'
                                                    )
                                                )
                                                .catch(error =>
                                                    returnError(error)
                                                );
                                        });
                                    })
                                    .catch(error => returnError(error));
                            })
                            .catch(error => {
                                returnError(error);
                                if (type === 'manual') {
                                    ctx.reply(messages.errorRetry)
                                        .then(() => dailyVote(ctx, chat))
                                        .catch(error => returnError(error));
                                } else {
                                    dailyVote(ctx, chat);
                                }
                            });
                    } else {
                        dailyVote(ctx, chat);
                    }
                });
        } else {
            if (type === 'manual') {
                chat.updated = 0;
                updateDB().then(() =>
                    ctx
                        .reply(messages.playersNotFound)
                        .catch(error => returnError(error))
                );
            }
        }
    });
};

const sort = players => {
    return new Promise(resolve => {
        const sorted = [
            ...players.sort((a, b) => (a.score > b.score ? 1 : -1)).reverse()
        ];

        if (sorted.length > 10) sorted.length = 10;

        resolve(sorted);
    });
};

const run = (ctx, type = 'auto') => {
    const date = new Date(ctx.update.message.date * 1000);

    getChat(ctx, type).then(({ chat }) => {
        if (hasPlayers(ctx, chat, type)) {
            if (chat.players.length) {
                const lastRun = chat.updated
                    ? new Date(chat.updated)
                    : new Date(0);
                const eta = 24 - Math.floor((date - lastRun) / 1000 / 60 / 60);

                const runVote = () => {
                    chat.updated = date;

                    updateDB().then(() => {
                        ctx.reply(messages.successRun)
                            .then(() => dailyVote(ctx, chat, type))
                            .catch(error => returnError(error));
                    });
                };

                if (type === 'manual') {
                    if (eta > 0) {
                        ctx.replyWithHTML(
                            messages.errorRunETA.replace('%s', eta)
                        ).catch(error => returnError(error));
                    } else {
                        runVote();
                    }
                } else {
                    if (!(eta > 0)) runVote();
                }
            } else {
                if (type === 'manual') {
                    ctx.reply(messages.playersNotFound).catch(error =>
                        returnError(error)
                    );
                }
            }
        }
    });
};

const init = ctx => {
    const msg = ctx.update.message;
    const currentChatId = msg.chat.id;
    const from = msg.from;

    if (from.id === currentChatId) {
        ctx.reply(
            messages.greetings.replace('%s', returnUserName(from, 'name')) +
                `\n${messages.greetingsError}`
        ).catch(error => returnError(error));
    } else {
        const initChat = () => {
            return new Promise(resolve => {
                if (!db.length || !db.find(({ id }) => id === currentChatId)) {
                    db.push({
                        id: currentChatId,
                        updated: 0,
                        players: []
                    });
                    updateDB().then(() =>
                        resolve('Chat is successfully initialized!')
                    );
                } else {
                    resolve('Chat is already initialized!');
                }
            });
        };

        ctx.getChatMember(from.id)
            .then(member => {
                if (member.user.is_bot) {
                    ctx.reply(messages.accessDenied).catch(error =>
                        returnError(error)
                    );
                } else {
                    initChat().then(() => {
                        ctx.reply(
                            messages.greetings.replace(
                                '%s',
                                returnUserName(from, 'name')
                            ) +
                                '\n\n' +
                                'Команды для управления ботом:' +
                                '\n' +
                                '/psinit - Инициализация и команды' +
                                '\n' +
                                '/psrun - Запуск голосования' +
                                '\n' +
                                '/psreset - Удалить игроков в чате' +
                                '\n' +
                                '/psjoin - Присоединиться к игре' +
                                '\n' +
                                '/pscancel - Отменить участие в игре' +
                                '\n' +
                                '/pstop - ТОП-10 игроков' +
                                '\n' +
                                '/pslist - Список игроков'
                        ).catch(error => returnError(error));
                    });
                }
            })
            .catch(error => {
                returnError(error).then(() =>
                    ctx.reply(messages.error).catch(error => returnError(error))
                );
            });
    }
};

bot.start(ctx => init(ctx));

bot.command('psinit', ctx => init(ctx));

bot.command('psrun', ctx => run(ctx, 'manual'));

bot.command('psreset', ctx => {
    getChat(ctx).then(({ chat, member }) => {
        if (isAdmin(member)) {
            chat.players = [];
            chat.updated = 0;
            updateDB().then(() =>
                ctx
                    .reply(messages.successReset)
                    .catch(error => returnError(error))
            );
        } else {
            ctx.reply(messages.accessDenied).catch(error => returnError(error));
        }
    });
});

bot.command('psjoin', ctx => {
    getChat(ctx).then(({ chat, member }) => {
        if (hasPlayers(ctx, chat)) {
            if (chat.players.length) {
                getPlayer(ctx, chat, member).then(player => {
                    if (player) {
                        if (player.status) {
                            ctx.reply(
                                messages.alreadyJoin.replace(
                                    '%s',
                                    returnUserName(member.user)
                                )
                            ).catch(error => returnError(error));
                        } else {
                            player.status = true;
                            player.name = returnUserName(member.user, 'name');

                            updateDB().then(() => {
                                ctx.reply(
                                    messages.successBack.replace(
                                        '%s',
                                        returnUserName(member.user)
                                    )
                                ).catch(error => returnError(error));
                            });
                        }
                    } else {
                        join(ctx, chat, member);
                    }
                });
            } else {
                join(ctx, chat, member);
            }
        }
    });
});

bot.command('pscancel', ctx => {
    getChat(ctx).then(({ chat, member }) => {
        if (hasPlayers(ctx, chat)) {
            if (chat.players.length) {
                getPlayer(ctx, chat, member).then(player => {
                    if (player) {
                        if (player.status) {
                            player.status = false;

                            updateDB().then(() => {
                                ctx.reply(
                                    messages.successLeave.replace(
                                        '%s',
                                        returnUserName(member.user)
                                    )
                                ).catch(error => returnError(error));
                            });
                        } else {
                            ctx.reply(
                                messages.alreadyLeave.replace(
                                    '%s',
                                    returnUserName(member.user)
                                )
                            ).catch(error => returnError(error));
                        }
                    } else {
                        ctx.reply(messages.hasNotData).catch(error =>
                            returnError(error)
                        );
                    }
                });
            } else {
                ctx.reply(messages.hasNotData).catch(error =>
                    returnError(error)
                );
            }
        }
    });
});

bot.command('pstop', ctx => {
    getChat(ctx).then(({ chat }) => {
        if (hasPlayers(ctx, chat)) {
            if (chat.players.length) {
                printPlayers(ctx, chat, 'top');
            } else {
                ctx.reply(messages.hasNotData).catch(error =>
                    returnError(error)
                );
            }
        }
    });
});

bot.command('pslist', ctx => {
    getChat(ctx).then(({ chat }) => {
        if (hasPlayers(ctx, chat)) {
            if (chat.players.length) {
                printPlayers(ctx, chat, 'all');
            } else {
                ctx.reply(messages.hasNotData).catch(error =>
                    returnError(error)
                );
            }
        }
    });
});

bot.on('message', ctx => {
    if (!ctx.update.message.text.match('/')) run(ctx);
});

bot.catch(error => returnError(error));

bot.launch();
